"use strict"
//OneSignal Code
var onesignal = {
    pushTag: function (tag_name, tag_value) {
        if (OneSignal) {
            OneSignal.push(function () {
                OneSignal.sendTag(tag_name, tag_value).catch(function () {
                    var OneSignal_unsynced_tags_store = new Widely.utilities._utils.idbKeyval.Store('OneSignal', 'unsynced-tags');
                    Widely.utilities._utils.idbKeyval.set(tag_name, tag_value, OneSignal_unsynced_tags_store);
                    if (window.SyncManager) {
                        _serviceWorker.ready.then(function (reg) {
                            OneSignal.getUserId(
                                function (player_id) {
                                    _serviceWorker.controller.postMessage({
                                        action: "oneSignalDataSync",
                                        app_id: OneSignal.config.appId,
                                        player_id: player_id
                                    });
                                }
                            )

                            reg.sync.register('oneSignalAnalyticsSync');
                        })
                    }
                });
            })
        }
    }
}
module.exports = onesignal;
