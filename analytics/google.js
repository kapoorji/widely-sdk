"use strict"
var google = {
    pushTag: function (tag_name, tag_value) {
        if (ga) {
            fetch('https://www.google-analytics.com/collect?v=1&&t=event&tid=' + ga.getAll()[0].get('trackingId') + '&cid=' + ga.getAll()[0].get('clientId') + '&ec=general&el=' + tag_name + '&ea=' + tag_value).then(function () {
                console.log('sent')
            }).catch(function () {
                console.log("Unable to send Google Analytics data");
                var GoogleAnalytics_unsynced_tags_store = new Widely.utilities._utils.idbKeyval.Store('GoogleAnalytics', 'unsynced-tags');
                Widely.utilities._utils.idbKeyval.set(tag_name, tag_value, GoogleAnalytics_unsynced_tags_store);
                if (window.SyncManager) {
                    _serviceWorker.ready.then(function (reg) {
                        _serviceWorker.controller.postMessage({
                            action: "googleAnalyticsDataSync",
                            client_id: ga.getAll()[0].get('clientId'),
                            tracking_id: ga.getAll()[0].get('trackingId')
                        })

                        reg.sync.register('googleAnalyticsSync');
                    })
                }

            });
        }
    }
}
module.exports = google;
