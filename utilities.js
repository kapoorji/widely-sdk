"use strict"
var utilities = {
    _componentThemes: {
        'default': {
            'background_color': '#444',
            'font_color': '#fff',
            'btn_color': '#fd586f',
            'font_family': 'sans-serif',
            'bell_color': '#02CEFF'
        },
        'basic': {
            'background_color': '#fff',
            'font_color': '#444',
            'btn_color': '#444',
            'font_family': 'times new roman',
            'bell_color': '#444'
        }
    },

    _updateBoxEl: {
        container: '',
        allow: '',
        deny: '',
        addListener: {
            allow: function (worker) {
                if (Widely.utilities._updateBoxEl.allow && typeof (Widely.utilities._updateBoxEl.allow) === 'object') {
                    Widely.utilities._updateBoxEl.allow.addEventListener('click', function (event) {
                        //
                        event.preventDefault();
                        worker.postMessage({ action: 'skipWaiting' });
                        console.log('updated');
                        Widely.utilities._utils.removeClass(Widely.utilities._updateBoxEl.container, 'prohive-hidden', true);
                        Widely.utilities._utils.addClass(Widely.utilities._updateBoxEl.container, 'prohive-hidden');
                        // Widely.utilities._utils.addClass(document.getElementById('confirmUpdate'), 'prohive-hidden');
                        //
                    });
                }
            },
            deny: function () {
                if (Widely.utilities._updateBoxEl.deny && typeof (Widely.utilities._updateBoxEl.deny) === 'object') {
                    Widely.utilities._updateBoxEl.deny.addEventListener('click', function (event) {
                        event.preventDefault();
                        console.log('Update dismissed');
                        Widely.utilities._utils.removeClass(Widely.utilities._updateBoxEl.container, 'prohive-hidden', true);
                        Widely.utilities._utils.addClass(Widely.utilities._updateBoxEl.container, 'prohive-hidden');
                        //
                    });
                }
            }
        }
    },

    // _subscribeEl: {
    //     container: '',
    //     allow: '',
    //     deny: '',
    //     addListener: {
    //         deny: function () {
    //             if (Widely.utilities._subscribeEl.deny && typeof (Widely.utilities._subscribeEl.deny) === 'object') {
    //                 Widely.utilities._subscribeEl.deny.addEventListener('click', function () {
    //                     if (Widely.utilities._subscribeEl.container) {
    //                         Widely.utilities._utils.removeClass(Widely.utilities._subscribeEl.container, 'prohive-hidden');
    //                         Widely.utilities._utils.addClass(Widely.utilities._subscribeEl.container, 'prohive-hidden');
    //                     }
    //                     var date = new Date().getTime();
    //                     console.log(date);
    //                     localStorage.setItem('progresshive::n_permission', date);
    //                 });
    //             }
    //         }

    //     }
    // },
    _exceptions: {
        ConfigException: function (errorMessage) {
            this.errorMessage = errorMessage;
            this.name = "ConfigException";
        }
    },

    _userModel: {
        // getRandomInt: function (min, max) {
        //     min = Math.ceil(min);
        //     max = Math.floor(max);
        //     return Math.floor(Math.random() * (max - min)) + min;
        // },

        // getUrl: function (url) {
        //     return url.split(/[?]/)[0];
        // },

        // getSegment: function () {
        //     var res_device = Widely.utilities._userInsights.getDevice();
        //     if (res_device && typeof (res_device) === 'object' && res_device.hasOwnProperty('device')) {
        //         var device = res_device.device || 'unknown';
        //     }
        //     else {
        //         var device = res_device;
        //     }
        //     if (res_device && typeof (res_device) === 'object' && res_device.hasOwnProperty('os')) {
        //         var os = res_device.os;
        //     }

        //     var browser = Widely.utilities._userInsights.getBrowser();
        //     var locale = Widely.utilities._userInsights.getLocale();
        //     return {
        //         browser: browser || 'unknown',
        //         device: device || 'unknown',
        //         locale: locale || 'unknown',
        //         os: os || 'unknown'
        //     }
        // },
        isStandAlone: function (res) {
            var standalone = Widely.utilities._userModel.getStandAlone();
            if (standalone == 'standalone' || standalone == 'fullscreen') {


                return true
            }
            else {

                return false
            }
        },
        getStandAlone: function (res) {
            if (global.matchMedia('(display-mode: standalone)').matches) {
                return "standalone";
            } else if (global.matchMedia('(display-mode: fullscreen)').matches) {
                return "fullscreen";
            } else {
                return "browser";
            }
        },

        // openIndexedDB: function (data) {
        //     return new Promise(function (resolve, reject) {
        //         var dbName = data.dbName;
        //         var osName = data.osName;
        //         var ver = data.ver || 1;
        //         if ('indexedDB' in global && typeof (dbName) !== 'undefined' && typeof (osName) !== 'undefined' && typeof (ver) !== 'undefined') {
        //             console.log('indexDB supported');
        //             var db;
        //             var indexDBrequest = global.indexedDB.open(dbName, ver);
        //             indexDBrequest.onsuccess = function (evt) {
        //                 db = evt.target.result;
        //                 resolve(db);
        //             };
        //             indexDBrequest.onerr = function (evt) {
        //                 console.log('err opaning indexDB');
        //                 reject('err');
        //             };
        //             indexDBrequest.onupgradeneeded = function (evt) {
        //                 db = evt.target.result;
        //                 if (!db.objectStoreNames.contains(osName)) {
        //                     db.createObjectStore(osName, { autoIncrement: true });
        //                 }
        //                 console.log(" just got upgraded");
        //                 // resolve(db);
        //             };
        //         }
        //         else {
        //             console.log('no indexDB not supported/or not db not initialized correctly');
        //             reject('err');
        //         }
        //     })

        // },
        // pushIndexedDb: function (data, db, osName) {
        //     return new Promise(function (resolve, reject) {
        //         var transaction = db.transaction([osName], "readwrite");
        //         var store = transaction.objectStore(osName);
        //         var request = store.add(data);
        //         request.onerror = function (evt) {
        //             console.log("not able to add data in indexDB", evt.target.error.name);
        //             reject(evt.target.error.name);
        //         }
        //         request.onsuccess = function () {
        //             console.log('data added successfully in indexDB');
        //             resolve(true);
        //         }
        //     })
        // },

        // getCookie: function (cname) {
        //     var name = cname + "=";
        //     var ca = document.cookie.split(';');
        //     for (var i = 0; i < ca.length; i++) {
        //         var c = ca[i];
        //         while (c.charAt(0) == ' ') {
        //             c = c.substring(1);
        //         }
        //         if (c.indexOf(name) == 0) {
        //             return c.substring(name.length, c.length);
        //         }
        //     }
        //     return "";
        // },
    },

    _trackServiceWorker: {
        updateReady: function (worker) {
            if (_serviceWorker.controller != null) {
                if (Widely.params.update_type == 'force') {
                    worker.postMessage({ action: 'skipWaiting' });
                }
                else if (Widely.params.update_type == 'major') {
                    Widely.utilities._utils.removeClass(Widely.utilities._updateBoxEl.container, 'prohive-hidden', true);

                    //
                    Widely.utilities.Widely.utilities._updateBoxEl.addListener.allow(worker);
                    Widely.utilities.Widely.utilities._updateBoxEl.addListener.deny();
                }
                else {
                    worker.postMessage({ 'action': 'set-config', 'params': Widely.params });
                    // _pushMessenger.pushNotificationState();
                }

            }
            else {
                worker.postMessage({ 'action': 'set-config', 'params': Widely.params });
            }
        },
        trackInstalling: function (worker) {
            worker.addEventListener('statechange', function () {
                if (worker.state == 'installed') {

                    Widely.utilities._trackServiceWorker.updateReady(worker);
                    //
                    //show cacheBox
                    //
                    var cacheBoxEl = document.getElementById('cacheBox');
                    Widely.utilities._utils.removeClass(cacheBoxEl, 'prohive-hidden', true);
                    global.setTimeout(function () {
                        Widely.utilities._utils.removeClass(cacheBoxEl, 'prohive-hidden', true);
                        Widely.utilities._utils.addClass(cacheBoxEl, 'prohive-hidden');
                    }, 3000);
                }
            });
        }
    },

    _utils: {
        // functions equivalent to addClass & removeClass jquery methods 
        //DONE: Start of functions



        hasClass: function (el, className) {
            if (el && typeof (el) === 'object' && typeof (className) === 'string' && className.length) {
                if (el.classList)
                    return el.classList.contains(className)
                else
                    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
            }
        },

        addClass: function (el, className) {
            if (el && typeof (el) === 'object' && typeof (className) === 'string' && className.length) {
                if (el.classList)
                    el.classList.add(className)
                else if (!Widely.utilities._utils.hasClass(el, className))
                    el.className += " " + className
            }
        },

        removeClass: function (el, className, popUp) {
            if (el && typeof (el) === 'object' && typeof (className) === 'string' && className.length) {
                var isPopUp = popUp || false;
                if (isPopUp) {
                    Widely.utilities._utils.checkForBox(el);
                }
                if (el.classList)
                    el.classList.remove(className)
                else if (Widely.utilities._utils.hasClass(el, className)) {
                    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
                    el.className = el.className.replace(reg, ' ')
                }
            }
        },
        extends: function () {

            // Variables
            var extended = {};
            var deep = true;
            var i = 0;
            var length = arguments.length;

            // Check if a deep merge
            if (Object.prototype.toString.call(arguments[0]) === '[object Boolean]') {
                deep = arguments[0];
                i++;
            }

            // Merge the object into the extended object
            var merge = function (obj) {
                for (var prop in obj) {
                    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                        // If deep merge and property is an object, merge properties
                        if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                            extended[prop] = Widely.utilities._utils.extends(true, extended[prop], obj[prop]);
                        } else {
                            extended[prop] = obj[prop];
                        }
                    }
                }
            };

            // Loop through each object and conduct a merge
            for (; i < length; i++) {
                var obj = arguments[i];
                merge(obj);
            }

            return extended;

        },
        //DONE: End of functions
        checkForBox: function (item) {
            if (item && typeof (item) === 'object') {
                if (Widely.params.popup_element) {
                    var el = document.getElementById(Widely.params.popup_element);
                    return el;
                } else {
                    var el = document.getElementById('_progresshive_popups');
                }
                var el_offset = 0;
                // for(var node of el.childNodes){
                for (var i = 0; i < el.childNodes.length; i++) {
                    var el_node = el.childNodes[i];
                    if (el_node.id === 'prohive-note' || el_node.id === 'connectionCheck' || el_node.id === 'cacheBox') {
                        if (!Widely.utilities._utils.hasClass(el_node, 'prohive-hidden')) {
                            el_offset = el_offset + (el_node.offsetHeight + 10);
                        }
                    }
                }
                if (item.id !== 'allowNotification') {
                    item.style.marginBottom = '' + el_offset + 'px';
                }
            }
        },

        showElement: function (id) {
            var element = document.getElementById(id);

            if (element !== null) {

                element.style.display = "block";
            }
        },

        hideElement: function (id) {
            var element = document.getElementById(id);

            if (element !== null)
                element.style.display = "none";
        },

        hasAlreadySeen: function () {

            return localStorage.getItem('progresshive::addToHomeClicked') || false;
        },

        getParameterByName: function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },

        // addOverlay: function() {
        //     document.body.innerHTML += '<div style="position: fixed; display: none; width: 100%; height: 100%; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(40,116,240,.95); z-index: 9999; cursor: pointer;" id="overlay" > <svg style="position: absolute; right: 1em; top: 1%; width: 21%;" width="81" height="91" id="arrow" viewBox="0 0 81 91" xmlns="http://www.w3.org/2000/svg"><g stroke-linecap="round" stroke="#fff" fill="none" fill-rule="evenodd"><path d="m79.42 12.832l-6.558-10.91-9.242 8.69" stroke-width="1.7"/><path d="m72.5 4s-4.36 71.902-71 86" stroke-width="2" stroke-linejoin="round"/></g></svg> <div style="position:relative; width:65%; color: #fff; top:7em; left:3em; font-family: sans-serif;" id="text"> <div style="font-size: 19px; font-weight: 400; font-family: sans-serif;" class="add_home_screen">Add '+Widely.params.views.addToHome.brandName+' to Homescreen</div><div class="content"> <p>Tap <i class="fa fa-ellipsis-v"></i> to bring up your browser menu and select \'Add to homescreen\' to pin the Widely web app.</p></div><div style="display: inline-block; background: #fff; padding: 10px 20px; border-radius: 2px; margin-top: 21px; text-transform: uppercase; font-size: 14px; font-weight: 500; color: #565656;" id="got_it" class="got_it">Got it!</div></div></div>';
        // },

        // detectMobileAndChrome: function () {
        //     var md = new MobileDetect(global.navigator.userAgent);
        //     var browser = Widely.utilities._userInsights.getBrowser();
        //     Widely.utilities._utils.hideElement(Widely.params.views.addToHome.iconId);
        //     if (md.mobile() !== null && (browser === "Chrome" || browser === "Opera")) {
        //         return true;
        //     }
        // },

        localStorageExists: function (item) {
            if (!localStorage.getItem(item) || localStorage.getItem(item) === null) {
                return false;
            }
            else {
                return true;
            }
        },

        addLocalStorage: function (key, value) {
            localStorage.setItem(key, value);
        },

        getLocalStorage: function (key) {
            return localStorage.getItem(key);
        }
    },

    // _userInsights : {
    //     getLocale: function () {
    //         var locale;
    //         if (navigator.languages !== undefined) {
    //             locale = navigator.languages[0];
    //             // console.log("language " + locale);
    //         }
    //         else {
    //             locale = navigator.language;
    //             console.log("in else " + navigator.language);
    //         }
    //         return locale;
    //     },
    //     getDevice: function () {
    //         // return navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i);
    //         var md = new MobileDetect(global.navigator.userAgent);
    //         var mobile = md.mobile();//return phone/tablet -model if matched,if not matched then return unknownPhone/unknownTablet 
    //         var phone = md.phone();

    //         //check is mobile/tablet or not 
    //         if (mobile == null || mobile == 'UnknownPhone' || mobile == 'UnknownTablet' || mobile == 'UnknownMobile') {
    //             // if not mobile/tablet
    //             var toreturn_mobile = 'desktop';
    //             return toreturn_mobile;
    //         }
    //         //mobile/tablet
    //         else {
    //             //check if device is a phone or not
    //             if (phone == null || phone == 'UnknownPhone' || phone == 'UnknownTablet' || phone == 'UnknownMobile') {
    //                 //if not phone means tablet
    //                 var tablet = md.tablet();//return tablet type
    //                 if (tablet !== 'UnknownTablet') {
    //                     var os = md.os();// return operating system on the device
    //                     var toreturn_phone = tablet;
    //                     return { device: toreturn_phone, os: os };
    //                 } else return null;

    //             }
    //             else {
    //                 // device is phone 

    //                 var os_device = md.os();
    //                 var toreturn_device = phone;
    //                 return { device: toreturn_phone, os: os };
    //             }
    //         }
    //     },
    //     getBrowser: function () {

    //         // return navigator.appCodeName;
    //         var md = new MobileDetect(global.navigator.userAgent);
    //         var browser;
    //         //check if device is a mobile or desktop
    //         if ((md.mobile() || md.mobile() == 'UnknownPhone' || md.mobile() == 'UnknownTablet' || md.mobile() == 'UnknownMobile')) {
    //             var md = new MobileDetect(global.navigator.userAgent);
    //             browser = md.userAgent();
    //             console.log(browser);
    //             return browser;
    //         }
    //         else {
    //             //device is a desktop

    //             //check for firefox
    //             if (md.match('Firefox/') && !md.match('Seamonkey/')) {
    //                 //device is using firefox
    //                 browser = 'Firefox';
    //                 return browser;
    //             }

    //             //check for chrome
    //             if (md.match('Chrome/') && !md.match('Chromium/') && !(md.match('OPR/') || md.match('Opera/'))) {
    //                 //device is using chrome
    //                 browser = 'Chrome';
    //                 return browser;
    //             }

    //             //check for Safari
    //             if (md.match('Safari/') && !(md.match('Chrome/') || (md.match('Chromium/')))) {
    //                 //device is using Safari
    //                 browser = 'Safari';
    //                 return browser;
    //             }

    //             //check for Opera
    //             if (md.match('OPR/') || md.match('Opera/')) {
    //                 //device is using Opera
    //                 browser = 'Opera';
    //                 return browser;
    //             }
    //             browser = null;
    //             return browser;
    //         }
    //     }

    // },

    _defaultBindings: {
        installation: global.addEventListener('beforeinstallprompt', function (e) {
            // e.userChoice will return a Promise.
            // TODO : Add usecase for notsubscribed users as well
            // DOUBT : Firebase call check

            if (Widely.params.views.addToHome.deferred == 'yes') {
                Widely.params.views.addToHome.prompt = e;
                e.preventDefault();
            }
            else if (Widely.params.views.addToHome.deferred == 'both' || Widely.params.views.addToHome.deferred == 'no') {
                // Widely.utilities._defaultBindingscaptureUserChoice(e);
                e.userChoice.then(function (choiceResult) {

                    if (choiceResult.outcome == 'dismissed') {
                        console.log('User cancelled home screen install');
                        Widely.utilities._utils.hideElement(Widely.params.views.addToHome.iconId);
                        if (Widely.analytics.oneSignal.pushTag) {
                            Widely.analytics.oneSignal.pushTag("PWAInstalled", "dismissed");
                        }
                        if (Widely.analytics.google.pushTag) {
                            Widely.analytics.google.pushTag("PWAInstalled", "dismissed");
                        }

                    }
                    else {
                        console.log('User added to home screen');
                        Widely.utilities._utils.hideElement(Widely.params.views.addToHome.iconId);
                        Widely.utilities._utils.addLocalStorage('progresshive::addToHomeDoneNative', true);
                        if (Widely.analytics.oneSignal.pushTag) {
                            Widely.analytics.oneSignal.pushTag("PWAInstalled", "accepted");
                        }
                        if (Widely.analytics.google.pushTag) {
                            Widely.analytics.google.pushTag("PWAInstalled", "accepted");
                        }

                    }

                });
            }
        }),

        // hashChange: global.addEventListener("hashchange", function () {
        //     // SPA specific change

        // }),

        // captureUserChoice: function (e) {
        // }

    },

    _appendElement: {
        popUpConatinerEl: function () {
            if (Widely.params.popup_element) {
                var el = document.getElementById(Widely.params.popup_element);
                return el;
            } else {
                var el = document.createElement("div");
                el.setAttribute("id", "_progresshive_popups");
                document.body.appendChild(el);
                return el;
            }
        },
    }
}
utilities._subscription = Object.freeze(require('./subscription_plan.js'));
utilities._utils.idbKeyval = require('idb-keyval/dist/idb-keyval-cjs-compat.min.js');
module.exports = utilities;