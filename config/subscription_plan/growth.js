var subscription_plan = {
    plan_name: "Growth",
    duration: "365 Days",
    platforms: ["Chromium", "Firefox", "Edge", "Safari"],
    Features: ["PWA", "Custom Caching", "Offline Availability", "WebShare", "Basic Analytics", "Push Notifications", "Custom Theme", "Performance Optimisations"],
    Support: ["Docs", "E-Mail", "Phone"]
}
module.exports = subscription_plan;