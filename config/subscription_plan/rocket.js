var subscription_plan = {
    plan_name: "Rocket",
    duration: "365 Days",
    platforms: ["Chromium", "Firefox", "Edge", "Safari"],
    Features: ["PWA", "Custom Caching", "Offline Availability", "WebShare", "Advanced Analytics", "Push Notifications", "Custom Theme", "Performance Optimisations", "Native Apps", "UX Suggestions"],
    Support: ["Docs", "E-Mail", "Phone"]
}
module.exports = subscription_plan;