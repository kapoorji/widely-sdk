var subscription_plan = {
    plan_name: "Free",
    duration: "15 Days",
    platforms: ["Chromium", "Firefox", "Edge"],
    Features: ["PWA", "Smart Caching", "Offline Availability", "WebShare"],
    Support: ["Docs", "E-Mail"]
}
module.exports = subscription_plan;