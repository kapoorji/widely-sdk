//List all the modules with there path here
var modulesList = {
    core: './core.js',
    defaults_free: './defaults/defaults_free.js',
    defaults_growth: './defaults/defaults_growth.js',
    defaults_rocket: './defaults/defaults_rocket.js',
    defaults_shopify_free: './defaults/defaults_shopify_free.js',
    utilities: './utilities.js',
    gestureBasedNavigation: './features/gestureBasedNavigation.js',
    webShare: './features/webShare.js',
    offlineBadgeJS: './ux/badges/offlineBadge.js',
    offlineBadgeCSS: './ux/badges/style/offlineBadge.css',
    poweredByWidelyJS: './ux/badges/poweredByWidelyBadge.js',
    poweredByWidelyLeftCSS: './ux/badges/style/poweredByWidelyLeft.css',
    poweredByWidelyRightCSS: './ux/badges/style/poweredByWidelyRight.css',
    updateBadge: './ux/badges/updateBadge.js',
    addToHomeScreenLayout: './ux/layouts/addToHomeScreenLayout.js',
    googleAnalytics: './analytics/google.js',
    oneSignalAnalytics: './analytics/oneSignal.js'
}
module.exports = modulesList;