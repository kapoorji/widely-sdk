const gulp = require('gulp');
const webpack = require('webpack-stream');
const rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var header = require('gulp-header');
const clean = require('gulp-clean');
const fileExists = require('file-exists');
var parameterized = require('gulp-parameterized');
var handlebars = require('gulp-compile-handlebars');

const modulesList = require('./config/modules.js');
const webpackConfigPath = './webpack.config.js';

var freePlanPreBuildTasksList = ['copy-core', 'copy-defaults', 'copy-utilities', 'copy-offlineBadge', 'copy-poweredByWidely', 'copy-addToHomeScreenLayout', 'copy-webShare'];

var growthPlanPreBuildTasksList = ['copy-core', 'copy-defaults', 'copy-utilities', 'copy-offlineBadge', 'copy-poweredByWidely', 'copy-updateBadge', 'copy-addToHomeScreenLayout', 'copy-gestureBasedNavigation', 'copy-googleAnalytics', 'copy-oneSignalAnalytics', 'copy-webShare'];

var rocketPlanPreBuildTasksList = ['copy-core', 'copy-defaults', 'copy-utilities', 'copy-offlineBadge', 'copy-poweredByWidely', 'copy-updateBadge', 'copy-addToHomeScreenLayout', 'copy-gestureBasedNavigation', 'copy-googleAnalytics', 'copy-oneSignalAnalytics', 'copy-webShare'];

var shopifyFreePlanPreBuildTasksList = ['copy-core', 'copy-defaults', 'copy-utilities', 'copy-offlineBadge', 'copy-addToHomeScreenLayout', 'copy-webShare'];



function copyModule(module_name, module_new_name) {
    var destination = './temp';
    if (!module_name) {
        console.error("Missing parameter: 'module_name'");
        return;
    }
    if (!module_new_name) {
        console.error("Missing parameter: 'module_new_name'");
        return;
    }
    var modulePath = modulesList[module_name];

    if (!modulePath) {
        console.error("Invalid module name: '" + module_name + "'\nHave you registered the module in ./config/modules.js ?")
        return;
    }
    return gulp.src(modulePath)
        .pipe(rename(module_new_name))
        .pipe(gulp.dest(destination));

}

function copySubscriptionConfig(subscription_plan) {
    var destination = './temp';
    return gulp.src('./config/subscription_plan/' + subscription_plan + '.js')
        .pipe(rename('subscription_plan.js'))
        .pipe(gulp.dest(destination));
}


gulp.task('set-plan', parameterized(function (done, params) {
    var subscription_plan = params.plan || 'free';
    process.env.plan = subscription_plan;
    if (fileExists.sync('./config/subscription_plan/' + subscription_plan + '.js')) {
        copySubscriptionConfig(subscription_plan);
    }
    else {
        console.log('Failed: ' + subscription_plan + ' plan file not found')
    }
    done();
}))

gulp.task('copy-core', function (done) {
    copyModule('core', 'core.js');
    done();
})

gulp.task('copy-defaults', function (done) {
    if (fileExists.sync('./defaults/params_user.js')) {
        modulesList['params_user'] = './defaults/params_user.js';
        copyModule('params_user', 'defaults.js');
    }
    else {
        var currentPlan = process.env.plan;
        copyModule('defaults_' + currentPlan, 'defaults.js');
    }
    done();
})

gulp.task('copy-utilities', function (done) {
    copyModule('utilities', 'utilities.js');
    done();
})

gulp.task('copy-gestureBasedNavigation', function (done) {
    copyModule('gestureBasedNavigation', 'gestureBasedNavigation.js');
    done();
})

gulp.task('copy-offlineBadge', function (done) {
    copyModule('offlineBadgeJS', 'offlineBadge.js');
    copyModule('offlineBadgeCSS', 'offlineBadge.css');
    done();
})

gulp.task('copy-webShare', function (done) {
    copyModule('webShare', 'webShare.js');
    done();
})

gulp.task('copy-poweredByWidely', function (done) {
    copyModule('poweredByWidelyJS', 'poweredByWidelyBadge.js');
    copyModule('poweredByWidelyLeftCSS', 'poweredByWidelyLeft.css');
    copyModule('poweredByWidelyRightCSS', 'poweredByWidelyRight.css');
    done();
})

gulp.task('copy-updateBadge', function (done) {
    copyModule('updateBadge', 'updateBadge.js');
    done();
})

gulp.task('copy-addToHomeScreenLayout', function (done) {
    copyModule('addToHomeScreenLayout', 'addToHomeScreenLayout.js');
    done();
})

gulp.task('copy-googleAnalytics', function (done) {
    copyModule('googleAnalytics', 'google.js');
    done();
})

gulp.task('copy-oneSignalAnalytics', function (done) {
    copyModule('oneSignalAnalytics', 'oneSignal.js');
    done();
})

gulp.task('clean', function () {
    return gulp.src('./tmp', { read: false })
        .pipe(clean());
});


gulp.task('build-free', parameterized.series("set-plan --plan free", gulp.parallel(freePlanPreBuildTasksList)), function () {
    return gulp.src('./temp/core.js')
        .pipe(webpack(webpackConfigPath))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-growth', parameterized.series("set-plan --plan growth", gulp.parallel(growthPlanPreBuildTasksList)), function () {
    return gulp.src('./temp/core.js')
        .pipe(webpack(webpackConfigPath))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-rocket', parameterized.series("set-plan --plan rocket", gulp.parallel(rocketPlanPreBuildTasksList)), function () {
    return gulp.src('./temp/core.js')
        .pipe(webpack(webpackConfigPath))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-shopify_free', parameterized.series("set-plan --plan shopify_free", gulp.parallel(shopifyFreePlanPreBuildTasksList)), function () {
    return gulp.src('./temp/core.js')
        .pipe(webpack(webpackConfigPath))
        .pipe(gulp.dest('./dist'));
});

gulp.task('build-js', parameterized(function (done, params) {

    var license = '/* \n @license Widely \n\n Terms: https://widely.io/terms-of-use \n\n */\n';
    var hbsFiles = './dist/bundle.hbs';
    var jsDest = './dist';
    var templateData = {
        customerDomain: params.domain
    }
    gulp.src(hbsFiles)
        .pipe(handlebars(templateData))
        .pipe(rename('widely.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(uglify({
            compress: {
                drop_console: true
            }
        }))
        .pipe(header(license))
        .pipe(rename('widely.min.js'))
        .pipe(gulp.dest(jsDest))
    done();
})
)