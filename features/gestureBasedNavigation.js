"use strict"
var installHammerJS = function () {
    //check if on standalone or not, activate only if on standalone
    if (!Widely.utilities._userModel.isStandAlone()) {
        return;
    }
    // on load check if Hammer.JS should be installed
    var isSafariOnIos = /(safari)/i.test(navigator.userAgent) && /(iPhone)/i.test(navigator.userAgent) || /(iPad)/i.test(navigator.userAgent);
    //if Widely.params.gestureBasedNavigation.install === 'iOS', set the parameter it to 'true' if the client is Safari browser on iOS else set it to false 
    if (Widely.params.gestureBasedNavigation.install === 'iOS') {
        Widely.params.gestureBasedNavigation.install = isSafariOnIos;
    }
    //install Hammer.JS if Widely.params.gestureBasedNavigation.install === true
    if (Widely.params.gestureBasedNavigation.install === true) {
        //create <script> tag to link Hammer.JS library
        var hammerJSScript = document.createElement('script');
        hammerJSScript.setAttribute('src', 'https://hammerjs.github.io/dist/hammer.min.js');
        hammerJSScript.setAttribute("type", "text/javascript");
        document.head.appendChild(hammerJSScript);
        hammerJSScript.onload = function () {
            var body = document.body;
            var hammerJS = new Hammer(body);
            console.log('installed');
            hammerJS.on('swiperight', function () {
                //go back if user swipes right
                global.history.back();
                console.log('back');
            })
            hammerJS.on('swipeleft', function () {
                //go forward if users swipes forward
                global.history.forward();
            })
        }
    }
    global.Widely.installHammerJS.gestureBasedNavigationLayout = require('../ux/layouts/gestureBasedNavigationLayout.js');
    global.Widely.installHammerJS.gestureBasedNavigationLayout();
}
module.exports = installHammerJS;