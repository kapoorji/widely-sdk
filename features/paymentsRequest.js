"use-strict"
function requestPayment(currency, value) {
    if (!window.PaymentRequest) {
        //Payment request API not supported, fallback to traditional checkout
    }
    else {
        //Payment request API supported
        const supportedPaymentMethods = ['basicCards', 'payPal', 'applePay', 'googlePay', 'samsungPay'];

        //Payment Object for each supported payment methods
        const basicCardsPaymentMethod = {
            supportedMethods: 'basic-card',
        };

        const paymentDetails = {
            total: {
                label: 'Total Amount',
                amount: {
                    currency: currency,
                    value: value
                }
            }
        };

        const options = {};

        var payMe = new PaymentRequest(
            supportedPaymentMethods,
            paymentDetails,
            options
        );
        payMe.show();
    }
}