"use strict"
var webShare = {
    trigger: function (data) {
        console.log(data)
        var dataToShare = data || {
            title: document.getElementsByTagName('title')[0].innerHTML || document.querySelector('[property="og:title"]').content || document.querySelector('[property="twitter:title"]').content || '',
            text: 'Hey! Check this out ' + document.querySelector('[name="description"]').content || document.querySelector('[property="og:description"]').content || document.querySelector('[property="twitter:description"]').content || '',
            url: location.href,
        };
        if (navigator.share) {
            navigator.share(dataToShare);
        }
    },
    attach: function (elementSelector) {
        var element = document.querySelector(elementSelector);
        if (element) {
            element.addEventListener('click', function () {
                Widely.webShare.trigger()
            });
        }
    }
}
module.exports = webShare;