"use strict"
// Settings live here, and these are their defaults
var _defaults = {
    'service_worker_url': './service-worker.js',
    'service_worker_scope': '/',
    'update_type': 'force',
    'gestureBasedNavigation': {
        'install': false,
        'showGuide': false,
    },
    'views': {
        'theme': {
            'type': 'default',
            'custom': {
                'background_color': '#fff',
                'font_color': '#444',
                'btn_color': '#444',
                'font_family': 'times new roman',
                'bell_color': '#444'
            }
        },

        'install': false,
        'offline': {
            'type': 'popUp',
            'content': 'You are working offline....',
            'position': 'bottom-left'
        },
        'cache': false,
        'updateBox': false,
        'badge': {
            'position': 'left',
            'display': true,
            'timePeriod': 3,
            'link': 'https://widely.io/?utm_source=clients&utm_medium=basic&utm_campaign=viralMarketing'
        },
        'addToHome': {
            'brandName': 'this web app',
            'brandColor': 'rgba(2,206,255,0.9);',
            'iconId': 'widelyPWAInstallIcon',
            'deferred': 'both',
            'reAppearLength': 3,
            'iOSLayout': false,
            'textColor': 'rgb(0,0,0);',
            'prompt': null
        },
    },
};
module.exports = _defaults;