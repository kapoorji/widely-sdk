"use strict"
var addOfflineBadge = function () {

    //CSS For the 'Offline' Badge
    var styling = require('./offlineBadge.css').toString();

    var testDiv = Widely.utilities._appendElement.popUpConatinerEl();
    var theme = Widely.params.views.theme.type;
    if (theme == 'custom') {
        Widely.utilities._componentThemes[theme] = Widely.params.views.theme.custom;
    }

    var style = document.createElement("style");
    style.innerHTML = styling;
    document.head.appendChild(style);

    if (Widely.params.views.offline) {
        if (Widely.params.views.offline.type === 'popUp') {
            // var oldDiv = document.getElementById('popUpContainer');
            switch (Widely.params.views.offline.position) {
                case 'top-middle': {
                    var alignItems = 'flex-start';
                    var justifyContent = 'center';
                    var top = '0%';
                    var transform = 'translateY(0%)';
                };
                    break;
                case 'top-left': {
                    var alignItems = 'flex-start';
                    var justifyContent = 'flex-start';
                    var top = '0%';
                    var transform = 'translateY(0%)';
                };
                    break;
                case 'top-right': {
                    var alignItems = 'flex-start';
                    var justifyContent = 'flex-end';
                    var top = '0%';
                    var transform = 'translateY(0%)';
                };
                    break;
                case 'bottom-middle': {
                    var alignItems = 'flex-end';
                    var justifyContent = 'center';
                    var top = '100%';
                    var transform = 'translateY(-100%)';
                };
                    break;
                case 'bottom-left': {
                    var alignItems = 'flex-end';
                    var justifyContent = 'flex-start';
                    var top = '100%';
                    var transform = 'translateY(-100%)';
                };
                    break;
                case 'bottom-right': {
                    var alignItems = 'flex-end';
                    var justifyContent = 'flex-end';
                    var top = '100%';
                    var transform = 'translateY(-100%)';
                };
                    break;
                default: {
                    var alignItems = 'center';
                    var justifyContent = 'center';
                }
            }

            if (window.matchMedia("(min-width: 600px)").matches) {
                var width = '20%';
            }
            else {
                var width = '100%';
            }
            var connectionShow = document.createElement("style");

            var connectionContainer = document.createElement("div");
            connectionContainer.setAttribute("id", "connectionCheck");
            connectionContainer.setAttribute("class", "prohive-hidden");
            connectionContainer.setAttribute("style", "position: fixed;top: " + top + "; transform: " + transform + "; left: 0;width: 100%;z-index: 10000; align-items:" + alignItems + "; justify-content:" + justifyContent + "; pointer-events:none; display:flex;");
            var connectionInsideDiv = document.createElement("div");
            connectionInsideDiv.setAttribute("style", "min-width:250px; width: " + width + "; background-color:" + Widely.utilities._componentThemes[theme]['background_color'] + ";color:" + Widely.utilities._componentThemes[theme]['font_color'] + "; text-align:center; font-size: 14px; font-family: " + Widely.utilities._componentThemes[theme]['font_family'] + "; border-radius:0.25rem; box-shadow: 0px 2px 6px rgba(0,0,0,.8); padding: 10px 0px 0px 0px;");
            var connectionMsgBox = document.createElement("p");
            var connectionMsg = document.createTextNode(Widely.params.views.offline.content);
            connectionMsgBox.appendChild(connectionMsg);
            connectionInsideDiv.appendChild(connectionMsgBox);
            connectionContainer.appendChild(connectionInsideDiv);

            testDiv.appendChild(connectionShow);
            testDiv.appendChild(connectionContainer);
            if (!navigator.onLine) {
                var el1 = document.getElementById('connectionCheck');
                el1.classList.remove('prohive-hidden');
            }
            else {
                var el1 = document.getElementById('connectionCheck');
                el1.classList.add('prohive-hidden');
            }

            global.addEventListener('online', function () {
                var el1 = document.getElementById('connectionCheck');
                el1.classList.add('prohive-hidden');
            });

            global.addEventListener('offline', function () {

                var el1 = document.getElementById('connectionCheck');
                el1.classList.remove('prohive-hidden');
            });

        }
    }
}
module.exports = addOfflineBadge;