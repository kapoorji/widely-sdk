"use strict"
var addUpdateBadge = function () {
    if (Widely.params.views.updateBox) {

        if (Widely.params.views.updateBox.type === 'popUp') {

            var card = document.createElement("div");
            card.setAttribute("id", "prohive-note");
            card.setAttribute("class", "prohive-hidden");
            card.setAttribute("style", "background-color: " + Widely.utilities._componentThemes[theme]["background_color"] + "; position: fixed;bottom: 0;left: 0;z-index: 10000;text-align: center !important;display: block;margin-bottom: 0.75rem;border: 1px solid #e5e5e5;border-radius: 0.25rem;font-family: " + Widely.utilities._componentThemes[theme]["font_family"] + ";box-shadow: 0px 2px 6px rgba(0,0,0,.8);line-height: 1.5;min-width: 250px;max-width: 300px;");

            var cardBody = document.createElement("div");
            cardBody.setAttribute("style", "padding: 1.25rem; display:block;");


            var cardHeading = document.createElement("h5");
            cardHeading.setAttribute("style", "margin-top: 0px; color:" + Widely.utilities._componentThemes[theme]["font_color"] + ";")
            var headContent = document.createTextNode("New Version Available");
            cardHeading.appendChild(headContent);

            var cardUpdate = document.createElement("a")
            // cardUpdate.setAttribute("id", "confirmUpdateBtn");
            cardUpdate.setAttribute("id", "prohive-update");
            cardUpdate.setAttribute("style", "color: " + Widely.utilities._componentThemes[theme]["btn_color"] + ";text-decoration: none; padding-right: 1rem !important; cursor:pointer!important;");
            //cardUpdate.setAttribute("onclick", "_appendElement.confirmUpdate()");
            var cardUpdateContent = document.createTextNode("Update");
            cardUpdate.appendChild(cardUpdateContent);

            var cardDismiss = document.createElement("a")
            cardDismiss.setAttribute("id", "prohive-dismiss");
            cardDismiss.setAttribute("style", "color: " + Widely.utilities._componentThemes[theme]["btn_color"] + ";text-decoration: none; cursor:pointer!important;");
            var cardDismissContent = document.createTextNode("Dismiss");
            cardDismiss.appendChild(cardDismissContent);

            cardBody.appendChild(cardHeading);
            cardBody.appendChild(cardUpdate);
            cardBody.appendChild(cardDismiss);
            card.appendChild(cardBody);

            // document.body.insertBefore(card, testDiv);
            testDiv.appendChild(card);

            _updateBoxEl.container = document.getElementById('prohive-note');
            _updateBoxEl.allow = document.getElementById('prohive-update');
            _updateBoxEl.deny = document.getElementById('prohive-dismiss');

        }

    }
}
module.exports = addUpdateBadge;