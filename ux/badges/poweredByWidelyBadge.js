"use strict"
var addPoweredByWidelyBadge = function () {
    if (Widely.params.views.badge && Widely.params.views.badge.display) {

        //CSS For the 'Powered By Widely' Badge
        var stylingLeft = require('./poweredByWidelyLeft.css').toString();;
        var stylingRight = require('./poweredByWidelyRight.css').toString();;

        switch (Widely.params.views.badge.position) {
            case 'left':
                var widelyBadgeStyle = document.createElement('style');
                widelyBadgeStyle.id = 'widelyBadgeStyle';

                widelyBadgeStyle.innerHTML = stylingLeft + '#pn-card.show{visibility:visible;-webkit-animation:slideup .5s,slidedown .5s ' + Widely.params.views.badge.timePeriod + 's; ' + 'animation:slideup .2s,slidedown .5s ' + Widely.params.views.badge.timePeriod + 's;}';

                document.head.appendChild(widelyBadgeStyle);
                break;

            case 'right':
                var widelyBadgeStyle = document.createElement('style');
                widelyBadgeStyle.id = 'widelyBadgeStyle';

                widelyBadgeStyle.innerHTML = stylingRight + '#pn-card.show{visibility:visible;-webkit-animation:slideup .5s,slidedown .5s ' + Widely.params.views.badge.timePeriod + 's; ' + 'animation:slideup .2s,slidedown .5s ' + Widely.params.views.badge.timePeriod + 's;}';

                document.head.appendChild(widelyBadgeStyle);
                break;
        }


        var badgeContainerDiv = document.createElement('div');
        badgeContainerDiv.id = "pn-card";

        var badgeCardContainerDiv = document.createElement('div');
        badgeCardContainerDiv.className = "card-content";

        var badgeLogoContainerDiv = document.createElement('div');
        badgeLogoContainerDiv.className = "pnglogocontainer";

        //contains images in base64 encoding
        badgeLogoContainerDiv.innerHTML = "<img src='https://i.ibb.co/sJXJvLm/pwa-official-logo.png'/><img src='https://i.ibb.co/G7yHVRF/lightning-bolt.png'/>by<img src='https://i.ibb.co/mzLS9LM/widely-logo.png'/></a>";

        badgeCardContainerDiv.appendChild(badgeLogoContainerDiv);
        badgeContainerDiv.appendChild(badgeCardContainerDiv);
        var anchorTagForBadge = document.createElement('a');
        anchorTagForBadge.href = Widely.params.views.badge.link;
        anchorTagForBadge.target = "_target";
        anchorTagForBadge.rel = "noopener";
        anchorTagForBadge.appendChild(badgeContainerDiv);
        document.body.appendChild(anchorTagForBadge);

        // function hideBadge() {
        var hideBadge = function () {
            var pnCard = document.getElementById("pn-card");
            if (pnCard) {
                pnCard.classList.remove("show");
            }
            sessionStorage.setItem('badgeAlreadyShown', 'true');
        }

        global.addEventListener('offline', function () {
            var time = Widely.params.views.badge.timePeriod * 1000 + 500;
            var pnCard = document.getElementById("pn-card");
            if (pnCard) {
                pnCard.className = "show";
                Widely.utilities._utils.showElement("poweredByWidelyBadge");
                setTimeout(hideBadge, time);
                //Widely.utilities._utils.showElement("poweredByWidelyBadge");
                // Widely.utilities._utils.removeClass(el1,'prohive-hidden',true);
            }
        });
        global.addEventListener('online', function () {
            hideBadge();
            Widely.utilities._utils.showElement("poweredByWidelyBadge");
        });

        var badgeAlreadyShown = sessionStorage.getItem('badgeAlreadyShown');
        if ((Widely.utilities._userModel.getStandAlone() === 'standalone' && badgeAlreadyShown !== 'true') || Widely.utilities._utils.getParameterByName('widely_badge') === 'true') {
            var time = Widely.params.views.badge.timePeriod * 1000 + 500;
            var pnCard = document.getElementById("pn-card");
            if (pnCard) {
                pnCard.className = "show";
                setTimeout(hideBadge, time);
            }
        }
        if (!navigator.onLine) {
            var time = Widely.params.views.badge.timePeriod * 1000 + 500;
            var pnCard = document.getElementById("pn-card");
            Widely.utilities._utils.showElement("poweredByWidelyBadge");
            if (pnCard) {
                pnCard.className = "show";
                setTimeout(hideBadge, time);
            }
        }
    }
}
module.exports = addPoweredByWidelyBadge;