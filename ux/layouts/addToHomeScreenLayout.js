"use strict"
var displayAddToHomeScreen = function () {
    var isChromeOnAndroid = /(chrome)/i.test(navigator.userAgent) && /(android)/i.test(navigator.userAgent);
    var isSafariOnIos = /(safari)/i.test(navigator.userAgent) && /(iPhone)/i.test(navigator.userAgent) || /(iPad)/i.test(navigator.userAgent);

    if ((!isChromeOnAndroid && !isSafariOnIos) || Widely.utilities._userModel.isStandAlone()) {
        return;
    }

    //if above 2 condition fails,i.e. user is on iOS + Safari /Android + Chrome and not in standalone mode then:

    function addEventListenerToAddToHomeIcon() { //this function add event listener to 'add to home' icon
        document.getElementById(Widely.params.views.addToHome.iconId).addEventListener('click', function () {
            initiateAddToHome(Widely.params.views.addToHome.brandName);

        })
    };
    // this function displays 'add to home' overlay
    function initiateAddToHome(title) {
        if (Widely.analytics.oneSignal.pushTag) {
            Widely.analytics.oneSignal.pushTag("PWAInstalled", "attemptedViaIcon");
        }
        if (Widely.analytics.google.pushTag) {
            Widely.analytics.google.pushTag("PWAInstalled", "attemptedViaIcon");
        }
        var overlayDiv = document.createElement('div');
        overlayDiv.id = "addToHomeOverlay";
        overlayDiv.style = "color: " + Widely.params.views.addToHome.textColor + "; position: fixed; width: 100%; height: 100; display: block; top: 0; right: 0; bottom: 0; left: 0; z-index: 9999; background-color:" + Widely.params.views.addToHome.brandColor;
        var icon;
        if (isChromeOnAndroid) {
            icon = '<img style="width: 3vh;" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxnPgoJCQk8Y2lyY2xlIGN4PSIyNTYiIGN5PSIyNTYiIHI9IjY0IiBmaWxsPSIjRkZGRkZGIi8+CgkJCTxjaXJjbGUgY3g9IjI1NiIgY3k9IjQ0OCIgcj0iNjQiIGZpbGw9IiNGRkZGRkYiLz4KCQkJPGNpcmNsZSBjeD0iMjU2IiBjeT0iNjQiIHI9IjY0IiBmaWxsPSIjRkZGRkZGIi8+CgkJPC9nPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />';
            overlayDiv.innerHTML = "<svg style='position: absolute; right: 1em; top: 1%; width: 21%;' width='81' height='91' viewBox='0 0 81 91' id='arrow' xmlns='http://www.w3.org/2000/svg'><g stroke-linecap='round' stroke='#fff' fill='none' fill-rule='evenod'><path d='m79.42 12.832l-6.558-10.91-9.242 8.69' stroke-width='1.7'></path><path d='m72.5 4s-4.36 71.902-71 86' stroke-width='2' stroke-linejoin='round'></path></g></svg><div style = 'position: fixed; top: 20vh; left: 5vw'><h4 style='font-size: 19px; font-weight: 400; font-family: sans-serif'>Add " + title + " to Homescreen</h4><p>Tap" + icon + "to bring up your browser menu and select 'Add to homescreen' to install the " + title + " web app.</p><button id='gotIt' style='background-color: #f857a6; display: inline-block; padding: 10px 20px; border-radius: 2px; margin-top: 21px; text-transform: uppercase; font-size: 14px; font-weight: 500'>GOT IT!</button></div>"
        }
        if (isSafariOnIos) {
            //<img> tag with src set to iOS share button image in base64
            icon = '<img style="width: 3vh;" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ0MS4xMjEgNDQxLjEyMSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDQxLjEyMSA0NDEuMTIxOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij48Zz48Zz48cGF0aCBkPSJNMzc2LjY3NywxNzEuOTU0aC03MS45NjdjLTkuMzUsMC0xNyw3LjY1LTE3LDE3czcuNjUsMTcsMTcsMTdoNTQuNjgzdjIwMS4xNjdIODEuNzI3VjIwNS45NTRoNjAuMzVjOS4zNSwwLDE3LTcuNjUsMTctMTcgICAgcy03LjY1LTE3LTE3LTE3SDY0LjQ0NGMtOS4zNSwwLTE2LjcxNyw4LjUtMTYuNzE3LDE3Ljg1djIzNS43MzNjMCw5LjM1LDcuMzY3LDE1LjU4MywxNi43MTcsMTUuNTgzaDMxMi4yMzMgICAgYzkuMzUsMCwxNi43MTctNi4yMzMsMTYuNzE3LTE1LjU4M1YxODkuODA0QzM5My4zOTQsMTgwLjQ1NCwzODYuMDI3LDE3MS45NTQsMzc2LjY3NywxNzEuOTU0eiIgZmlsbD0iIzE0NzVkNiIvPjxwYXRoIGQ9Ik0yMTcuNzI3LDI5OS40NTRjOS4zNSwwLDE3LTcuNjUsMTctMTdWNTIuMTA0bDcyLjUzMyw2NC42YzMuMTE3LDIuODMzLDcuMzY3LDQuMjUsMTEuMzMzLDQuMjUgICAgYzQuODE3LDAsOS4zNS0xLjk4MywxMi43NS01LjY2N2M2LjIzMy03LjA4Myw1LjY2Ny0xNy44NS0xLjQxNy0yNC4wODNsLTk3Ljc1LTg2Ljk4M2MtNi4yMzMtNS4zODMtMTUuMy01LjY2Ny0yMS44MTctMC41NjcgICAgYy0zLjExNywxLjQxNy01LjY2NywzLjk2Ny03LjM2Nyw2LjhsLTg0LjcxNyw4MS4zMTdjLTYuOCw2LjUxNy03LjA4MywxNy4yODMtMC41NjcsMjQuMDgzYzYuNTE3LDYuOCwxNy4yODMsNy4wODMsMjQuMDgzLDAuNTY3ICAgIGw1OC45MzMtNTYuNjY3djIyMi43QzIwMC43MjcsMjkxLjgwNCwyMDguMzc3LDI5OS40NTQsMjE3LjcyNywyOTkuNDU0eiIgZmlsbD0iIzE0NzVkNiIvPjwvZz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PC9zdmc+" />';

            //HTML for overlay div
            overlayDiv.innerHTML = "<div style = 'position: fixed; top: 50vh; left: 50vw; transform: translate(-50%, -50%);'><h4 style='font-size: 19px; color: " + Widely.params.views.addToHome.textColor + ";  font-weight: 400; font-family: sans-serif'>Add " + title + " to Homescreen</h4><p>Tap" + icon + "to bring up your browser menu and select 'Add to homescreen' to install the " + title + " web app.</p><button id='gotIt' style='background-color: #f857a6; display: inline-block; padding: 10px 20px; border-radius: 2px; margin-top: 21px; text-transform: uppercase; color: white; font-size: 14px; font-weight: 500'>GOT IT!</button></div>";
        }
        document.body.appendChild(overlayDiv);

        //add event listener to the gotIt button
        var gotItButton = document.getElementById('gotIt');
        if (gotItButton) {
            gotItButton.addEventListener('click', function () {
                //hide overlay div if got it button is clicked, and create a localStorage item with value = reAppearLength
                var overlayDiv = document.getElementById('addToHomeOverlay');
                if (overlayDiv) {
                    document.body.removeChild(overlayDiv);
                }

                localStorage.setItem('avoidAddToHomeScreen', Widely.params.views.addToHome.reAppearLength);
                if (Widely.analytics.oneSignal.pushTag) {
                    Widely.analytics.oneSignal.pushTag("PWAInstalled", "addToHomeOverlayGotItClicked");
                }
                if (Widely.analytics.google.pushTag) {
                    Widely.analytics.google.pushTag("PWAInstalled", "addToHomeOverlayGotItClicked");
                }
            })
        }

        //addToHomeIcon.style.display = 'none';
    }


    //first get the value of 'avoidAddToHomeScreen' from localStorage, if it is not set than set it to 1 to display the icon for the first time
    var avoidAddToHomeScreen = parseInt(Widely.utilities._utils.getLocalStorage('progresshive:avoidAddToHomeScreen'));
    if (!avoidAddToHomeScreen) {
        avoidAddToHomeScreen = 1;
    }
    else {
        if (global.location.pathname === "/") {
            // if 'avoidAddToHomeScreen' is set, than decrease it's value by 1
            Widely.utilities._utils.addLocalStorage('progresshive:avoidAddToHomeScreen', avoidAddToHomeScreen - 1);
        }
    }

    //on load get iconId from parameters, if iconId is specified than add event listener to it, else check if user is on Safari  and iOS and add our own icon if he/she is.

    var userSpecifiedIcon = document.getElementById(Widely.params.views.addToHome.iconId);
    if (userSpecifiedIcon && avoidAddToHomeScreen === 1) {
        // addToHomeIcon = userSpecifiedIconId;
        Widely.utilities._utils.showElement(Widely.params.views.addToHome.iconId);
        addEventListenerToAddToHomeIcon();
    }
    else {
        if (avoidAddToHomeScreen === 1 && Widely.params.views.addToHome.iOSLayout) {
            //create 'add to home' icon
            var addToHomeIcon = document.createElement('div');
            addToHomeIcon.id = 'widelyAddToHomeIcon';
            addToHomeIcon.innerHTML = '<img src = "https://image.ibb.co/dL7Q8o/add_To_Homescreen.png" style = "width: 6vw">';
            addToHomeIcon.style.position = 'fixed';
            addToHomeIcon.style.right = '0vw';
            addToHomeIcon.style.bottom = '0vh';
            //addToHomeIcon.style.visibility = "hidden";

            document.body.appendChild(addToHomeIcon);
            Widely.params.views.addToHome.iconId = 'widelyAddToHomeIcon'; //if wrong iconId/no IconId is specified use widelyAddToHomeIcon' as the icon id
            addEventListenerToAddToHomeIcon();
        }
    }

}

module.exports = displayAddToHomeScreen;