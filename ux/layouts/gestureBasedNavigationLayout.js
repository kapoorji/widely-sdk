"use strict"
var gestureBasedNavigationLayout = function () {
    var guideShown = parseInt(localStorage.getItem('guideShown'));//guideShown variable records if the gesture navigation has been shown to user or not. It takes value '1' if shown

    //if Widely.params.gestureBasedNavigation.install === true and Widely.params.gestureBasedNavigation.showGuide and guide is not shown already
    if (Widely.params.gestureBasedNavigation.install === true && Widely.params.gestureBasedNavigation.showGuide === true && !guideShown) {

        var swipeLeft = 'https://i.ibb.co/cww9cJv/swipe-Left.gif';
        var swipeRight = 'https://i.ibb.co/CVbzQxT/swipe-Right.gif';

        //contains cancel icon in base64 encoding
        var cancelIcon = 'https://i.ibb.co/Tvd3rZt/cancel-Icon.png';

        var guideDiv = document.createElement('div');//div to contain user guide for gesture based navigation
        guideDiv.id = "addToHomeGuide";
        guideDiv.style = "color: " + Widely.params.views.addToHome.textColor + "; position: fixed; width: 100%; height: 100; display: block; top: 0; right: 0; bottom: 0; left: 0; z-index: 9999; background-color:" + Widely.params.views.addToHome.brandColor;

        guideDiv.innerHTML = "<img id = 'cancelIcon' style = 'width: 5vw; position: fixed; top: 1vh; right: 1vw;' src = " + cancelIcon + " ><div style = 'position: fixed; top: 25vh; left: 50vw; transform: translate(-50%, -50%);'><h4 style='font-size: 19px; color: " + Widely.params.views.addToHome.textColor + "; font-weight: 400; font-family: sans-serif' id = 'swipeGuideText'>Swipe left to move forward</h4><img id = 'swipeImg' style = 'width: 35vw;' src = " + swipeLeft + " ><button id='next1' style='background-color: #f857a6; display: inline-block; padding: 10px 20px; border-radius: 2px; margin-top: 21px; text-transform: uppercase; color: white; font-size: 14px; font-weight: 500'>Next</button></div>";

        document.body.appendChild(guideDiv);

        var cancelIcon = document.getElementById('cancelIcon');
        if (cancelIcon) {
            cancelIcon.addEventListener('click', function () {
                localStorage.setItem('guideShown', '1') //set guideShown to '1' if user clicks the cancel icon
                guideDiv.style.display = "none";
            })
        }

        var nextButton1 = document.getElementById('next1');
        if (nextButton1) {
            nextButton1.addEventListener('click', function () {
                var swipeImg = document.getElementById('swipeImg');
                if (swipeImg) {
                    swipeImg.src = swipeRight;
                }
                var swipeGuideText = document.getElementById('swipeGuideText');
                if (swipeGuideText) {
                    swipeGuideText.innerHTML = 'Swipe right to move backward';
                }
                nextButton1.id = 'next2';
                var nextButton2 = document.getElementById('next2');
                if (nextButton2) {
                    nextButton2.addEventListener('click', function () {
                        guideDiv = document.getElementById('addToHomeGuide');
                        if (guideDiv) {
                            localStorage.setItem('guideShown', '1')
                            guideDiv.style.display = "none";
                        }
                    })
                }
            })
        }
    }
}
module.exports = gestureBasedNavigationLayout;