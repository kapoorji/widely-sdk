window._serviceWorker = navigator.serviceWorker;
(function () {
    "use strict";
    var global = window;
    //Check client domain name simple check
    var dname = { value: "{{customerDomain}}" };

    if (global.location.hostname != dname.value) {
        return undefined;
    }
    var _iOSDevice = !!navigator.platform.match(/iPhone|iPod|iPad/);
    if (_iOSDevice && global.Widely.utilities._subscription.plan_name === 'free') {
        return undefined;
    }

    // Check browser support
    // This is done as early as possible, to make it as fast as possible for unsupported browsers
    // Requires ServiceWorker
    if (!_serviceWorker) {
        global.Widely = null;
        return undefined;
    }
    var Widely = {};
    Widely.pwa = function (settings) {
        // Set params provided by user
        if (Widely.utilities._userModel.getStandAlone() === 'standalone') {
            if (!Widely.utilities._utils.localStorageExists('progresshive::appInstalled')) {
                Widely.utilities._utils.addLocalStorage('progresshive::appInstalled', true);
            }
            if (Widely.analytics.oneSignal.pushTag) {
                Widely.analytics.oneSignal.pushTag("PWAInstalled", "appUser");
            }
            if (Widely.analytics.google.pushTag) {
                Widely.analytics.google.pushTag("PWAInstalled", "appUser");
            }
        }

        Widely.params = Widely.setParams(settings);

        // Register service worker
        if (Widely.params['service_worker_url']) {
            // register the service worker
            _serviceWorker.register(Widely.params['service_worker_url'], { scope: Widely.params['service_worker_scope'] }).then(function (reg) {

                // check local storage    
                // Widely.utilities._userModel.checkUser();

                console.log('ServiceWorker registration successful with scope: ' + reg.scope);


                // Send the settings to the ServiceWorker

                if (reg.waiting) {
                    Widely.utilities._trackServiceWorker.updateReady(reg.waiting);
                    return;
                }

                if (reg.installing) {
                    Widely.utilities._trackServiceWorker.trackInstalling(reg.installing);
                    return;
                }


                reg.addEventListener('updatefound', function () {
                    Widely.utilities._trackServiceWorker.trackInstalling(reg.installing);
                });

                var refreshing;

                // Check of the user is already subscribed or not

                //////////////////

            }).catch(function (err) {
                // registration failed :(

                console.log('ServiceWorker registration failed: %c' + err);
            });


        }
        else {
            // Throw exception for user, if they have not mentioned service-worker path.
            throw new utilities_exceptions.ConfigException("Service worker path undefined, provide a valid service_worker_url config value.");
        }
    };
    Widely.setParams = function (settings) {
        settings = settings || {};
        var params = Widely.utilities._utils.extends({}, _defaults, settings);
        console.log(params);
        return params;
    };
    global.Widely = Widely;
    window.addEventListener('load', function () {
        if (Widely.installHammerJS) {
            Widely.installHammerJS();
        }
        if (Widely.addOfflineBadge) {
            Widely.addOfflineBadge();
        }
        if (Widely.addPoweredByWidelyBadge) {
            Widely.addPoweredByWidelyBadge();
        }
        if (Widely.addUpdateBadge) {
            Widely.addUpdateBadge();
        }
        if (Widely.displayAddToHomeScreen) {
            Widely.displayAddToHomeScreen();
        }
    });
}).call(this)


_defaults = require('./defaults.js');
global.Widely.utilities = require('./utilities.js');
global.Widely.addOfflineBadge = require('./offlineBadge.js');
global.Widely.displayAddToHomeScreen = require('./addToHomeScreenLayout.js');
try {
    global.Widely.addPoweredByWidelyBadge = require('./poweredByWidelyBadge.js');
}
catch (e) {
    global.Widely.addPoweredByWidelyBadge = function () { return; }
}
try {
    global.Widely.webShare = require('./webShare.js');
}
catch (e) {
    global.Widely.webShare = {}
}
try {
    global.Widely.addUpdateBadge = require('./updateBadge.js');
}
catch (e) {
    global.Widely.addUpdateBadge = function () { return; }
}
try {
    global.Widely.installHammerJS = require('./gestureBasedNavigation.js');
}
catch (e) {
    global.Widely.installHammerJS = function () { return; }
}

try {
    global.Widely.analytics = {};
    global.Widely.analytics.google = require('./google.js');
}
catch (e) {
    global.Widely.analytics.google = {};
}

try {
    global.Widely.analytics.oneSignal = require('./oneSignal.js');
}
catch (e) {
    global.Widely.analytics.oneSignal = {};
}