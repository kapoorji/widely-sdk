
# Widely API
### Installation:
1. `git clone https://YOUR_USERNAME@bitbucket.org/kapoorji/api-modularization.git`
2. `npm install`

### Building the Widely SDK for a plan:
1.  `gulp build-plan_name`
2.  '`webpack`
3.  The value of `plan_name` for various plans is:

    | Widely Plan 	| plan_name 	|
    |-------------	|-------------	|
    | Free        	| free        	|
    | Growth      	| growth      	|
    | Rocket      	| rocket      	|
    | Shopify Free  | shopify_free  |


### Developer Notes:

1. **Adding a new module:**

    * Add the JavaScript/CSS file for the module in an appropriate sub-directory
    . Register the module in ./config/modules.js path by adding the `module_name:      module_path` in the object
    * Create a task in the gulpfile.js to copy the module to the temp folder:

	    ```
        gulp.task('copy-module_name', function(){
            copyModule('module_name', 'module_new_name_in_the_temp_folder');
            done();
        })
        ```

    * Add the module to pre-build tasks of the plans in which the module is needed, for example to add the module in the pre-build task of the free plan, add `copy-module_name` to the array `freePlanPreBuildTasksList`.

2. **Adding a new plan:**

    * Define a new task(if not defined), for each module needed in the plan, the task will copy the module to the temp folder:

	    ```
        gulp.task('copy-module_name', function(){
            copyModule('module_name', 'module_new_name_in_the_temp_folder');
            done();
        })
        ```
    
    * Create new array containing the pre-build task for the new plan, let's say that the name of the new plan is widely-plus, than:

	    ```
        var widelyPlusPlanPreBuildTasksList = ['set-plan-growth', 'copy-core', 'copy-defaults', 'copy-utilities', 'copy-offlineBadge', 'copy-poweredByWidely', 'copy-updateBadge', 'copy-addToHomeScreenLayout', 'copy-gestureBasedNavigation', 'copy-googleAnalytics', 'copy-oneSignalAnalytics', ......];
        ```
        
    * Create a new build task for the new plan:

	    ```
        gulp.task('build-widely-plus',gulp.parallel(widelyPlusPlanPreBuildTasksList), function () {
            return gulp.src('./temp/core.js')
                       .pipe(webpack(webpackConfigPath))
                       .pipe(gulp.dest('./dist'));
        });
        ```
	
	* Add the value of `plan_name` in the above table in this readme.md