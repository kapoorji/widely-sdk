module.exports = {
    entry: './temp/core.js',
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['css-loader']
            },
        ]
    },
    output: {
        filename: 'bundle.hbs'
    }
};